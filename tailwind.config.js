module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      'h1': ['Circular Std'], 
      'text': ['Gangster Grotesk'],

    },
    extend: {
      fontSize: {
        '7xl': ['4.35rem', '64px']
      },
      colors: {
        indigo: {
          450: '#1B031F',
        },
        pink: {
          450: '#FFDFF0',
        },
        black: {
          450: '#1B031F',
        },
        gray: {
          450: '#f5f5f5',
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
