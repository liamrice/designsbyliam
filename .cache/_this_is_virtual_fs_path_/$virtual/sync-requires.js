
// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---src-pages-404-js": preferDefault(require("/Users/liamrice/Documents/Sites/designsbyliam/src/pages/404.js")),
  "component---src-pages-about-js": preferDefault(require("/Users/liamrice/Documents/Sites/designsbyliam/src/pages/about.js")),
  "component---src-pages-contact-js": preferDefault(require("/Users/liamrice/Documents/Sites/designsbyliam/src/pages/contact.js")),
  "component---src-pages-index-js": preferDefault(require("/Users/liamrice/Documents/Sites/designsbyliam/src/pages/index.js"))
}

