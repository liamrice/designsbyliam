import React, { useState } from "react"
import styled from "styled-components"
import NavbarLinks from "./NavbarLinks"
import Logo from "./Logo"

const Navigation = styled.nav`
  height: 10vh;
  margin-top: 4rem;
  display: flex;
  align-items: center;
  position: relative;
  justify-content: space-between;
  z-index: 2;
  align-self: center;

  @media (max-width: 768px) {
    position: sticky;
    padding: 1rem 1rem!important;
    height: auto;
    top: 0;
    left: 0;
    right: 0;
    left: 0;
  }
`

const Toggle = styled.div`
  display: none;
  height: 100%;
  cursor: pointer;
  padding: 0 0vw;

  @media (max-width: 768px) {
    display: flex;
  }
`

const Navbox = styled.div`
  display: flex;
  height: 100%;
  justify-content: flex-end;
  align-items: center;

  @media (max-width: 768px) {
    flex-direction: column;
    position: fixed;
    width: 100%;
    justify-content: center;
    background-color: #fff;
    top: 0;
    transition: all 0.3s ease-in;
    left: ${props => (props.open ? "-100%" : "0")};
  }
`

const Hamburger = styled.div`
  z-index: 999;
  background-color: #111;
  width: 30px;
  height: 3px;
  transition: all .3s linear;
  align-self: center;
  position: relative;
  transform: ${props => (props.open ? "rotate(-45deg)" : "inherit")};

  ::before,
  ::after {
    width: 30px;
    height: 3px;
    background-color: #111;
    content: "";
    position: absolute;
    transition: all 0.3s linear;
  }

  ::before {
    transform: ${props =>
      props.open ? "rotate(-90deg) translate(-10px, 0px)" : "rotate(0deg)"};
    top: -10px;
  }

  ::after {
    opacity: ${props => (props.open ? "0" : "1")};
    transform: ${props => (props.open ? "rotate(90deg) " : "rotate(0deg)")};
    top: 10px;
  }
`
const Navbar = () => {
  const [navbarOpen, setNavbarOpen] = useState(false)

  return (
    <Navigation className="mx-6 my-6 lg:mx-28 lg:mt-22">
      <Logo />
      <Toggle
        navbarOpen={navbarOpen}
        onClick={() => setNavbarOpen(!navbarOpen)}
      >
        {navbarOpen ? <Hamburger open /> : <Hamburger />}
      </Toggle>
      {navbarOpen ? (
        <Navbox>
          <NavbarLinks />
          <button class="flex mt-5 bg-indigo-450 hover:bg-blue-700 text-white font-bold py-3 px-6 rounded-full">
           Get in touch 
           <svg class="mt-1.5 ml-2 fill-current" width="13" height="12" viewBox="0 0 13 12" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0.571548 5.43047C0.300721 5.46775 0.09198 5.70335 0.09198 5.98842C0.09198 6.29941 0.340399 6.55151 0.646839 6.55151L10.4 6.55151L6.87677 10.1126L6.82292 10.1757C6.6613 10.3958 6.67857 10.7086 6.87515 10.909C7.09138 11.1293 7.4427 11.13 7.65983 10.9106L12.128 6.39493C12.1535 6.37011 12.1767 6.34283 12.1971 6.31348C12.3511 6.09333 12.3306 5.78606 12.1357 5.5891L7.6598 1.06636L7.59746 1.01198C7.37989 0.84889 7.07168 0.867737 6.87512 1.06808C6.6589 1.28845 6.65965 1.64498 6.8768 1.86441L10.4009 5.42533L0.646839 5.42533L0.571548 5.43047Z"></path>
                         </svg>
           </button>
        </Navbox>
      ) : (
        <Navbox open>
          <NavbarLinks />
          <button class="flex ml-5 bg-gray-450 hover:bg-blue-700 text-indigo-450 font-bold py-3 px-6 rounded-full">
           Get in touch 
           <svg class="mt-1.5 ml-2 fill-current" width="13" height="12" viewBox="0 0 13 12" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0.571548 5.43047C0.300721 5.46775 0.09198 5.70335 0.09198 5.98842C0.09198 6.29941 0.340399 6.55151 0.646839 6.55151L10.4 6.55151L6.87677 10.1126L6.82292 10.1757C6.6613 10.3958 6.67857 10.7086 6.87515 10.909C7.09138 11.1293 7.4427 11.13 7.65983 10.9106L12.128 6.39493C12.1535 6.37011 12.1767 6.34283 12.1971 6.31348C12.3511 6.09333 12.3306 5.78606 12.1357 5.5891L7.6598 1.06636L7.59746 1.01198C7.37989 0.84889 7.07168 0.867737 6.87512 1.06808C6.6589 1.28845 6.65965 1.64498 6.8768 1.86441L10.4009 5.42533L0.646839 5.42533L0.571548 5.43047Z"></path>
                         </svg>
           </button>
        </Navbox>
      )}
    </Navigation>
  )
}

export default Navbar