import React from "react"
import styled from "styled-components"
import { Link, useStaticQuery, graphql } from "gatsby"

const LogoWrap = styled.div`
  margin: auto 0;
  flex: 0 1 36px;

  @media (max-width: 768px) and (orientation: landscape) {
    flex: 0 1 25px;
  }
`
const Logo = () => (
    
        <LogoWrap as={Link} to="/">
          <img style={{maxWidth: 81}} src="logo-v1.png"></img>
        </LogoWrap>
)

export default Logo