import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"

const NavItem = styled(Link)`
  text-decoration: none;
  display: inline-block;
  white-space: nowrap;
  margin: 0 1vw;
  transition: all 200ms ease-in;
  position: relative;

  :after {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    width: 0%;
    content: ".";
    color: transparent;
    background: pink;
    height: 1px;
    transition: all 0.4s ease-in;
  }

  :hover {
    color: pink;
    ::after {
      width: 100%;
    }
  }

  @media (max-width: 768px) {
    padding: 20px 0;
    font-size: 1.5rem;
    z-index: 6;
  }
`
const NavbarLinks = () => {
  return (
    <>
      <NavItem className="font-black text-gray-450 font-h1" to="/">Work</NavItem>
      <NavItem className="font-black text-gray-450 font-h1" to="/about/">About</NavItem>
    </>
  )
}

export default NavbarLinks