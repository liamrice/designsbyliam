// import React from "react"
// import { Link } from "gatsby"
// const ListLink = props => (
//   <li className="mx-5 text-lg font-black border-2 border-transparent font-h1 lg:hover:text-indigo-450 lg:hover:bg-transparent text-indigo-450 hover:bg-pink-450 hover:text-white" style={{ display: `inline-block`, marginRight: `1rem` }}>
//     <Link to={props.to}>{props.children}</Link>
//   </li>
// )

// export default function Layout({ children }) {
//   return (
//     <div style={{margin: `3rem 7rem`,}}>
//       <header style={{ marginBottom: `1.5rem`, display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
//         <Link to="/" style={{ textShadow: `none`, backgroundImage: `none` }}>
//           <img style={{display: 'inline', maxWidth: 81,}} src="/images/logo-v1.png" alt="designsbyliam logo"></img>
//         </Link>
//         <ul className="flex items-center" style={{ listStyle: `none`, float: `right` }}>
//           <ListLink to="/">Home</ListLink>
//           <ListLink to="/about/">About</ListLink>
//           <button class="flex ml-5 bg-indigo-450 hover:bg-blue-700 text-white font-bold py-3 px-6 rounded-full">
//           Get in touch 
//           <svg class="mt-1.5 ml-2 fill-current" width="13" height="12" viewBox="0 0 13 12" xmlns="http://www.w3.org/2000/svg">
//                             <path fill-rule="evenodd" clip-rule="evenodd" d="M0.571548 5.43047C0.300721 5.46775 0.09198 5.70335 0.09198 5.98842C0.09198 6.29941 0.340399 6.55151 0.646839 6.55151L10.4 6.55151L6.87677 10.1126L6.82292 10.1757C6.6613 10.3958 6.67857 10.7086 6.87515 10.909C7.09138 11.1293 7.4427 11.13 7.65983 10.9106L12.128 6.39493C12.1535 6.37011 12.1767 6.34283 12.1971 6.31348C12.3511 6.09333 12.3306 5.78606 12.1357 5.5891L7.6598 1.06636L7.59746 1.01198C7.37989 0.84889 7.07168 0.867737 6.87512 1.06808C6.6589 1.28845 6.65965 1.64498 6.8768 1.86441L10.4009 5.42533L0.646839 5.42533L0.571548 5.43047Z"></path>
//                         </svg>
//           </button>
//         </ul>
//       </header>
//       {children}
//     </div>
//   )
// }

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"

import Header from "./header"
import Navbar from "./navbar/Navbar"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <>
    <Navbar className="mt-20" />
      <div className="mx-4 my-4 lg:mx-28 lg:mt-22"
        style={{
          padding: `0px 1.0875rem 1.45rem`,
        }}
      >
        <main>{children}</main>
        <footer>
          © {new Date().getFullYear()}, Built with
          {` `}
          <a href="https://www.gatsbyjs.org">Gatsby</a>
        </footer>
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
